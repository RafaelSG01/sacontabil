@extends('layouts.site')

@section('content')
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  
  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="{{asset('images/Slide-sa.png')}}" alt="...">

    </div>
    <div class="item">
      <img src="{{asset('images/praia.jpg')}}" alt="...">

    </div>
    <div class="item">
      <img src="{{asset('images/Hub.jpg')}}" alt="...">
   
    </div>

  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <i class="fa fa-angle-left" aria-hidden="true"></i>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <i class="fa fa-angle-right" aria-hidden="true"></i>
    <span class="sr-only">Next</span>
  </a>
</div>

<!--serviços-->
<div class="service">
  <div class="container">
  <!--row -->
    <div class="row">
      <!--aside-->  
      <aside class="col-md-6 text-center"> 
      <i class="fa fa-file-text information-images" aria-hidden="true"></i>
          <p class="information-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo explicabo eligendi, esse quis deleniti nisi ipsam optio maiores! Possimus, suscipit sit dicta velit eveniet ad tempora animi saepe, eaque perferendis!</p>
      </aside>
      <!--- my article-->
      <article class="col-md-6 text-center">
      <i class="fa fa-line-chart information-images" aria-hidden="true"></i> 
          <p class="information-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores optio tempore at accusamus recusandae ipsum quaerat eligendi voluptatibus, assumenda quas vel eaque totam, esse nesciunt consequatur hic provident, autem dicta.</p>
      </article>
    </div>  
<!--row -->
    <div class="row">
      <!--aside-->  
      <aside class="col-md-6 text-center img"> 
      <i class="fa fa-check-square-o information-images" aria-hidden="true"></i>  
          <p class="information-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate tenetur nihil dicta deserunt, dolorem repellat maiores est quis molestiae recusandae quo, ea maxime eius consequuntur earum! Error, impedit ab deserunt.</p>
      </aside>
      <!--- my article-->
      <article class="col-md-6 text-center">
      <i class="fa fa-users information-images" aria-hidden="true"></i> 
          <p class="information-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur nemo neque porro tempora molestiae. Commodi error quos atque voluptate voluptates similique accusamus enim consequuntur esse omnis cum maiores dolor, quidem.</p>
      </article>
    </div>
    </div>  
  </div>

<!--Notícias-->
  <div class="new">
    <div class="container service">
    <p class="text-center news-label">
      Noticias
  </p>

    <div class="row">
      <div class="col-md-2 text-center data-target ">   
        <p>11</p>
        <p>NOV</p>
        <p>2017</p>
      </div>
      <div class="col-md-10">
        <h3 class="news-title">Notícia 1</h3> 
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores optio tempore at accusamus recusandae ipsum quaerat eligendi voluptatibus, assumenda quas vel eaque totam, esse nesciunt consequatur hic provident, autem dicta.</p>
      </div>
    </div>

    <div class="row">
      <div class="col-md-2 text-center data-target ">   
        <p>11</p>
        <p>NOV</p>
        <p>2017</p>
      </div>
      <div class="col-md-10">
        <h3 class="news-title">Notícia 2</h3> 
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores optio tempore at accusamus recusandae ipsum quaerat eligendi voluptatibus, assumenda quas vel eaque totam, esse nesciunt consequatur hic provident, autem dicta.</p>
      </div>
    </div>
  
    </div>
  </div>

<!--Usuários-->
  <div class="user">
    <div class="container">
    <p class="text-center news-label">Nossos Clientes</p>
  
      <!--aside-->  
      <aside class="col-md-3 text-center img-center">   
        <img src="{{asset('images/usuario.png')}}" alt="Imagem responsiva" class="img-circle img-responsive">
          <p class="user-name">Lorem</p>
      </aside>
      <!--- my article-->
      <article class="col-md-3 text-center img-center">  
        <img src="{{asset('images/usuario.png')}}" alt="Imagem responsiva" class="img-circle img-responsive">
          <p class="user-name">Lorem</p>
      </article>

      <!--aside-->  
      <aside class="col-md-3 text-center img-center">   
        <img src="{{asset('images/usuario.png')}}" alt="Imagem responsiva" class="img-circle img-responsive">
          <p class="user-name">Lorem</p>
      </aside>
      <!--- my article-->
      <article class="col-md-3 text-center img-center">  
        <img src="{{asset('images/usuario.png')}}" alt="Imagem responsiva" class="img-circle img-responsive">
          <p class="user-name">Lorem</p>
      </article>
    </div>  
  </div>



@endsection
