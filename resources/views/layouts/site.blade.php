<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- As 3 meta tags acima *devem* vir em primeiro lugar dentro do `head`; qualquer outro conteúdo deve vir *após* essas tags -->
    <title>SA Contabil</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">    
    <link href="https://fonts.googleapis.com/css?family=Abel" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">

    <!-- HTML5 shim e Respond.js para suporte no IE8 de elementos HTML5 e media queries -->
    <!-- ALERTA: Respond.js não funciona se você visualizar uma página file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
  <!-- Barra login -->

  <!-- Barra menu -->
  <nav class="navbar navbar-default">
    <div class="login-bar text-right">
      <div class="container">
        <a href="{{ url('/login') }}" class="login-button">
        <i class="fa fa-users" aria-hidden="true"></i>   
        Login
      </a>
      </div>
    </div>
    <hr>
    <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#"><img src="{{asset('images/logo-sa.png')}}" alt="..."></a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="#">Quem somos</a></li>
          <li><a href="#">Nossos serviços</a></li>
          <li><a href="#">Calendário de obrigações</a></li>
          <li><a href="#">Notícias</a></li>
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container -->
  </nav>

  @yield('content')
  
  <!--Barra de Informações-->
<div class="information">
  <div class="container">
  <!--row -->
    <div class="row">
      <!--aside-->  
      <aside class="col-md-3 text-left">   
          <p>Áreas Fiscal</p>
          <p>Previdenciária/Trabalhista e Área Contábil</p>
          <p>Telefone (85) 3345 1249</p>
          <p>Horário de Funcionamento: 8 horas às 17 horas</p>
          <p>Endereço: Rua Major Cícero Franklin, 1863</p>
          <p>Centro - Pacatuba(CE)</p>
          <p>cep: 61801-210</p>
          <p>E-mail: severianoassessoria@severianoassessoria.com.br</p>
      </aside>
      <!--- my article-->
      <article class="col-md-7 text-center">  

        <img src="{{asset('images/CONTABILIDADE.png')}}" class="img-accounting">
      </article>

      <!--aside-->  
      <aside class="col-md-2 text-left">
          <a href="" class="information-names">
            <p>Quam Somos</p>
          </a>
          <a href="" class="information-names">
            <p>Nossos Serviços</p>
          </a>
          <a href="" class="information-names">
            <p>Calendário de Obrigações</p>
          </a>
          <a href="" class="information-names">
            <p>Notícias</p>
          </a>
          <ul class="list-inline social-icons">
            <li><i class="fa fa-facebook-official" aria-hidden="true"></i></li>
            <li><i class="fa fa-instagram" aria-hidden="true"></i></li>
          </ul>
      </aside>
    </div>  
</div>

  <script src="{{ asset('js/jquery.min.js')}}"></script>
  <script src="{{ asset('js/bootstrap.min.js')}}"></script>
  <script type="$('.carousel').carousel()"></script>
  </body>
</html>